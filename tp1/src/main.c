/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include <stdio.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;
    uint8_t flag;
    printf("Sensor Entrada-> 1; Sensor Salida-> 2; Sensor Alarma->3\n\n");
    hw_Init();

    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {
        input = hw_LeerEntrada();
        
        if (input == SENSOR_1) {
            hw_AbrirBarrera();
           flag=1;
           }
        
        if (input == SENSOR_2) {
           if(flag==1){// Esperas menores para agilizar 
           printf("Esperando 5 seg\n\n");
           hw_Pausems(1500);
           while(hw_LeerEntrada() == SENSOR_1)
           
           {printf("Esperando 5 seg extra\n\n");
           hw_Pausems(1500);
           
           }
            hw_CerrarBarrera();
            flag=0;}
           else {hw_EncenderAlarma();
           flag=0;
           
           while(hw_LeerEntrada()!= SENSOR_3){
               hw_Pausems(1000);
               printf("Esperando a desactivar alarma\n");
               //hw_Pausems(500);
           }
           hw_ApagarAlarma();
           } 
       }
    }
    hw_Pausems(100);
    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/



#ifndef _GARAJE_H_
#define _GARAJE_H_

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef enum {
    STANDBY,
    INGRESO_FICHA,
    EST_TE,
    EST_CAFE,
    EST_SOUND
}FSM_CAFETERA_STATE;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/


void fsm_cafetera_init(void);

void fsm_cafetera_runCycle(void);

// Eventos
void fsm_cafetera_ev_IngresoFicha(void);
void fsm_cafetera_ev_eligeTe(void);
void fsm_cafetera_ev_eligeCafe(void);
void fsm_cafetera_evSalida_Cafe_on(void);
void fsm_cafetera_evSalida_Cafe_off(void);
void fsm_cafetera_evSalida_Te_on(void);
void fsm_cafetera_evSalida_Te_off(void);
void fsm_cafetera_evSound_on(void);
void fsm_cafetera_evSound_off(void);
void fsm_cafetera_evTick1seg(void);
void fsm_cafetera_evTick200ms(void);
void fsm_cafetera_evLed_on(void);
void fsm_cafetera_evLed_off(void);
void fsm_cafetera_evLedToggle(void);
// Debugging
void fsm_cafetera_printCurrentState(void);

/*==================[end of file]============================================*/
#endif 

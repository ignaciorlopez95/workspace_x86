#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT      27  // ASCII para la tecla Esc
#define INGRESAFICHA  49  // ASCII para la tecla 1
#define INGRESACAFE  50  // ASCII para la tecla 2
#define INGRESATE 51 // Tecla 3


// Funciones que configuran la consola de Linux como interfaz de I/O
void hw_Init(void);
void hw_DeInit(void);


// Funciones basicas para leer que entrada esta activa y pausar la ejecucion


void hw_Pausems(uint16_t t);

uint8_t hw_LeerEntrada(void);
void hw_IngresaFicha(void);
void hw_DevuelveFicha(void);
void hw_SalidaTe_on(void);
void hw_SalidaTe_off(void);
void hw_SalidaCafe_on(void);
void hw_SalidaCafe_off(void);
void hw_Sound_on(void);
void hw_Sound_off(void);
void hw_led_on(void);
void hw_led_off(void);
void hw_led_toggle(void);
/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */

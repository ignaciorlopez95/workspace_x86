/*==================[inclusions]=============================================*/

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

struct termios oldt, newt;

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

void hw_IngresaFicha(void)
{
    printf("Ficha ingresada\n\nEsperando a CAFE o TE\n\n");
    
}

void hw_DevuelveFicha(void)
{
    printf("Devolviendo Ficha \n\n");
}

void hw_SalidaCafe_on(void)
{
    printf("Salida Cafe ON \n\n");
}

void hw_SalidaCafe_off(void)
{
    printf("Salida Cafe OFF \n\n");
}

void hw_SalidaTe_on(void)
{
    printf("Salida Te ON \n\n");
}

void hw_SalidaTe_off(void)
{
    printf("Salida Te OFF \n\n");
}
void hw_Sound_on(void)
{
    printf("Indicador Sonoro ON \n\n");
}
void hw_Sound_off(void)
{
    printf("Indicador Sonoro OFF \n\n");
}
void hw_led_on(void){
    printf("Led On\n\n");
}

void hw_led_off(void){
    printf("Led Off\n\n");
}

void hw_led_toggle(void)
{   
    printf("Led On\n\n");
    printf("Led Off\n\n");

}

/*==================[end of file]============================================*/

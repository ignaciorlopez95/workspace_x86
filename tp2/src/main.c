/*==================[inclusions]=============================================*/

#include "main.h"
#include "fsm_cafetera.h"
#include "hw.h"
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    uint8_t input = 0;
    uint16_t cont_ms = 0;
    
    hw_Init();

    fsm_cafetera_init();

     printf("EN STANDBY\nINGRESAR FICHA TECLA 1\n\n");  

    while (input != EXIT) {
        
        input = hw_LeerEntrada();

       
        if (input == INGRESAFICHA) 
        {

            fsm_cafetera_ev_IngresoFicha();
       
        }

        if (input== INGRESATE) 
        {

            fsm_cafetera_ev_eligeTe();

        } else if (input== INGRESACAFE)
        {

            fsm_cafetera_ev_eligeCafe();

        }
            
        cont_ms++;

        if (cont_ms == 1000) 
        {

            cont_ms = 0;
            fsm_cafetera_evTick1seg();

        }
        
        if(cont_ms % 200==0)
        {

            fsm_cafetera_evTick200ms();

        }
        

        fsm_cafetera_runCycle();
        
      
        hw_Pausems(1);
    }

    hw_DeInit();

    return 0;
}


        
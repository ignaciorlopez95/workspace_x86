/*==================[inclusions]=============================================*/

#include "fsm_cafetera.h"
#include "hw.h"
#include <stdio.h>
#include <stdbool.h>

/*==================[macros and definitions]=================================*/

#define ESPERA_CAFE  15
#define ESPERA_TE 10
#define ESPERA_FICHA 3
#define ESPERA_SOUND 2


/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

FSM_CAFETERA_STATE state;

bool ev_IngresoFicha;
bool ev_EligeTe;
bool ev_EligeCafe;
bool ev_SalidaCafe_on;
bool ev_SalidaCafe_off;
bool ev_SalidaTe_on;
bool ev_SalidaTe_off;
bool ev_Sound_on;
bool ev_Sound_off;
bool evTick1seg_raised;
bool evTick200ms_raised;
bool ev_led_on;
bool ev_led_off;

uint8_t count_seg;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void clearEvents(void)
{
    ev_IngresoFicha=0;
    ev_EligeTe=0;
    ev_EligeCafe=0;
    ev_SalidaCafe_on=0;
    ev_SalidaCafe_off=0;
    ev_SalidaTe_on=0;
    ev_SalidaTe_off=0;
    ev_Sound_on=0;
    ev_Sound_off=0;
    evTick1seg_raised=0;
    evTick200ms_raised=0;
   
}

/*==================[external functions definition]==========================*/

void fsm_cafetera_init(void)
{

    state = STANDBY;
    clearEvents();

}

void fsm_cafetera_runCycle(void)
{
    
    switch (state) {
        case STANDBY:
            
            if (ev_IngresoFicha)
            {
                
                hw_IngresaFicha();
                state = INGRESO_FICHA;
                count_seg=0;
                
            }

            else if(evTick1seg_raised) /*En este caso no hay contador debido a estar en standby*/
            {

                hw_led_toggle();  
           
            }
            break;

       
        case INGRESO_FICHA:
        
            if (ev_EligeCafe) 
            {
               
                count_seg=0;
                hw_SalidaCafe_on();
                state = EST_CAFE;
            
            }
            if (ev_EligeTe)
            {
            
                hw_SalidaTe_on();
                state = EST_TE;
           
            }
            else if (evTick1seg_raised &&  (count_seg < ESPERA_FICHA)) 
            {
               
                count_seg++;
                hw_led_toggle();
                
            }
            else if(evTick1seg_raised && (count_seg == ESPERA_FICHA))
            {
             
                hw_DevuelveFicha();
                state=STANDBY;

            }
            
            break;

        
        case EST_TE:
            
             if (evTick1seg_raised && (count_seg < ESPERA_TE)) 
            {
                
                count_seg++;

            }
            else if (evTick1seg_raised && (count_seg == ESPERA_TE)) 
            {
               
                hw_SalidaTe_off();
                hw_Sound_on();
                count_seg=0;
                state = EST_SOUND;

            }
            
            else if(evTick200ms_raised && (count_seg < ESPERA_TE))
            {

                hw_led_toggle();

            }
            break;

       
        case EST_CAFE:
             
           if(evTick1seg_raised && (count_seg < ESPERA_CAFE))
           {
               
               count_seg++;

           }
           else if (evTick1seg_raised && (count_seg == ESPERA_CAFE))
           {
               
                hw_SalidaCafe_off();
                hw_Sound_on();
                count_seg=0;
                state = EST_SOUND;

           }
           
           else if(evTick200ms_raised && (count_seg < ESPERA_CAFE))
           {

              hw_led_toggle();

           }
           break;

       
        case EST_SOUND:

           if(evTick1seg_raised && (count_seg < ESPERA_SOUND))
           {

            count_seg++;

           }
           else if ( evTick1seg_raised && (count_seg == ESPERA_SOUND))
           {
          
            hw_Sound_off();
            printf("VUELTA A STDBY\n");
            state = STANDBY;

           }
           break;

    }

    clearEvents();
}



void fsm_cafetera_ev_IngresoFicha(void) 

{

    ev_IngresoFicha=1;

}

void fsm_cafetera_ev_eligeTe(void) 

{

    ev_EligeTe=1;

}

void fsm_cafetera_ev_eligeCafe(void)

 {

     ev_EligeCafe=1;

 }

void fsm_cafetera_evSalida_Cafe_on(void) 

{

    ev_SalidaCafe_on=1;
    
}

void fsm_cafetera_evSalida_Cafe_off(void)

{

     ev_SalidaCafe_off=1;

}

void fsm_cafetera_evSalida_Te_on(void) 

{

    ev_SalidaTe_on=1;
    
}

void fsm_cafetera_evSalida_Te_off(void) 

{

    ev_SalidaCafe_off=1;

}

void fsm_cafetera_evSound_on(void) 

{

    ev_Sound_on=1;

}

void fsm_cafetera_evSound_off(void)

{

    ev_Sound_off=1;

}

void fsm_cafetera_evTick1seg(void)
{

    evTick1seg_raised = 1;


}
void fsm_cafetera_evTick200ms(void)
{

    evTick200ms_raised=1;

}

void fsm_cafetera_printCurrentState(void)
{
    
    printf("Estado actual: %0d \n", state);

}

void fsm_cafetera_led_on(void)
{

    ev_led_on=1;

}
void fsm_cafetera_led_off(void)
{

    ev_led_off=1;

}
/*==================[end of file]============================================*/
